{
    const app = require('../app');
    const supertest = require("supertest");
    const chai = require('chai');

    chai.should();
    const request = supertest.agent(app);

    const testFunc = (query?: string, variables: any = {}) => {
        return request
        .post('/graphql')
        .set('Connetion', 'keep alive')
        .set('Content-Type', 'application/json')
        .send({query, variables})
    };

    module.exports = testFunc
}
