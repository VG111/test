export const updateProfile = `
    mutation updateProfile($profile: UserInput!) {
        updateProfile(profile: $profile) {
            msg
        }
    }
`
export const addPost = `
    mutation addPost($body: PostInput!) {
        addPost(body: $body) {
            msg
        }
    }
`
export const addChat = `
    mutation addChat($chat_user: String!, $current_user: String!) {
        addChat(chat_user: $chat_user, current_user: $current_user) {
            msg
        }
    }
    
`
export const addFriend = `
    mutation addFriend($user_id: String!, $current_user: String!) {
        addFriend(user_id: $user_id, current_user: $current_user) {
            _id
        }
    }
`

export const changeLike = `
    mutation changeLike($post: PostInput!){
        changeLike(post: $post) {
            msg
        }
    }
`

export const removeFriend = `
    mutation removeFriend($user_id: String!, $current_user: String!) {
        removeFriend(user_id: $user_id, current_user: $current_user) {
            _id
        }
    }
`