export const getAllPosts = `
    query($current_user: String!) {
        getAllPosts(current_user: $current_user) {
            _id
        }
    }
`
export const getUserPosts = `
    query($id: String!) {
        getUserPosts(id: $id) {
            _id
        }
    }
`
export const getFriends = `
    query($email: String!) {
        getFriends(email: $email) {
            _id
        }
    }
`
export const getChats = `
    query($email: String!) {
        getChats(email: $email) {
            chatUsersInfo {
                _id
            }
            chats {
                _id
            }
        }
    }
`
export const getUsers = `
    query($current_user: String!) {
        getUsers(current_user: $current_user) {
            _id
        }
    }
`
export const getPostComments = `
    query($postId: String!) {
        getPostComments(postId: $postId) {
            _id
        }
    }
`
export const getUserProfile = `
    query($profileId: String!) {
        getUserProfile(profileId: $profileId) {
            _id
        }
    }
`
