{
    const { getChats } = require('./queries')
    const { addChat } = require('./mutations')
    const testFunc = require('./requestFunc')


    describe('chat tests', () => {
        it('getChats test', (done) => {
            testFunc(getChats, {email: 'masterprovlad@gmail.com'})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getChats.chats.length.should.not.equal(0)
                done();
            });
        })
        // it('addChat test', (done) => {
        //     testFunc(addChat, {chat_user: 'some',current_user: 'some'})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.addChat.msg.length.should.not.equal(0)
        //         done();
        //     });
        // })
    })
}