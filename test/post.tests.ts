{
    const { getAllPosts, getUserPosts, getPostComments } = require('./queries')
    const { addPost, changeLike } = require('./mutations')
    const testFunc = require('./requestFunc')

    describe('post test', () => {
        it('getAllPosts test', (done) => {
            testFunc(getAllPosts, {current_user: 'masterprovlad@gmail.com'})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getAllPosts.length.should.not.equal(0)
                done();
            });
        })
        it('getUserPosts test', (done) => {
            testFunc(getUserPosts, {id: "6028103324849322b0bcc55d"})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getUserPosts.length.should.not.equal(0)
                done();
            });
        })
        // it('getPostComments test', (done) => {
        //     testFunc(getPostComments, {postId: "604f27efac3485068ced9469"})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.getPostComments.length.should.not.equal(0)
        //         done();
        //     });
        // })
        // it('addPost test', (done) => {
        //     testFunc(addPost, {body: {some: 'input'}})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.addPost.msg.length.should.not.equal(0)
        //         done();
        //     });
        // })
        // it('changeLike test', (done) => {
        //     testFunc(changeLike, {post: {some: 'input'}})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.changeLike.msg.length.should.not.equal(0)
        //         done();
        //     });
        // })
    })
}