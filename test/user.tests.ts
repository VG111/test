{
    const testFunc = require('./requestFunc')
    const { getUsers, getFriends, getUserProfile } = require('./queries')
    const { removeFriend, updateProfile, addFriend } = require('./mutations')

    describe('user tests', () => {
        it('getUsers test', (done) => {
            testFunc(getUsers, {current_user: 'masterprovlad@gmail.com'})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getUsers.length.should.not.equal(0)
                done();
            });
        })
        it('getFriends test', (done) => {
            testFunc(getFriends, {email: 'masterprovlad@gmail.com'})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getFriends.length.should.not.equal(0)
                done();
            });
        })
        it('getUserProfile test', (done) => {
            testFunc(getUserProfile, {profileId: "6028103324849322b0bcc55d"})
            .end((err:any, res:any) => {
                res.status.should.equal(200)
                res.body.data.getUserProfile._id.length.should.not.equal(0)
                done();
            });
        })
        // it('removeFriend test', (done) => {
        //     testFunc(removeFriend, {user_id: 'some', current_user: 'some'})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.removeFriend.msg.length.should.not.equal(0)
        //         done();
        //     });
        // })
        // it('updateProfile test', (done) => {
        //     testFunc(updateProfile, {profile: {some: 'input'}})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.updateProfile.msg.length.should.not.equal(0)
        //         done();
        //     });
        // })
        // it('addFriend test', (done) => {
        //     testFunc(addFriend, {user_id: 'some',current_user: 'some'})
        //     .end((err:any, res:any) => {
        //         res.status.should.equal(200)
        //         res.body.data.addFriend._id.length.should.not.equal(0)
        //         done();
        //     });
        // })
    })
}